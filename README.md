# Backup Repositories

**Description**:  Simple Python 3.6 script, designed to run on AWS Lambdas, that backs up Gitlab Repositories onto an S3.

# Install

1. Upload code to Lambda. Set the following environment variables:
   - *GITLABTOKEN*: token with programmatic access to API
   - *GITURL*: full url, including credentials, to gitlab (e.g. https://username:passsword@gitlab.com/)
   - *GITLABGROUP*: name of group to be backed up
   - *BUCKET*: destination bucket

2. Ensure that the Lambda has appropriate permissioning.

# Open source licensing info
[LICENSE](LICENSE)
