from botocore.vendored import requests
import boto3
import os

def download_branch(repo_path, repo_id, branch_name, branch_sha, timestamp):
    
    apiurl = "https://gitlab.com/api/v4/projects/"+repo_id+"/repository/archive.tar.gz?sha="+branch_sha
    directory = timestamp + "/" + repo_path + "/"
    filename = directory + branch_name+"-"+branch_sha+".tar.gz"

    headers = {
        'Private-Token': os.environ['GITLABTOKEN'],
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }
   
    if not os.path.exists("/tmp/"+directory):
        os.makedirs("/tmp/"+directory)
    
    r = requests.request("GET", apiurl, headers=headers)
    with open("/tmp/"+filename, 'wb+') as f:
        for chunk in r.iter_content(chunk_size=8192): 
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)       
    
    s3 = boto3.client('s3')
    bucket = os.environ['BUCKET']
    s3path = 'repo/'
    s3.upload_file("/tmp/" + filename, bucket, s3path + filename)
    
    return print('apiurl: ' + apiurl)
