import json
from botocore.vendored import requests
import os

def get_branches(id):
    
    apiurl = "https://gitlab.com/api/v4/projects/"+id+"/repository/branches"

    headers = {
        'Private-Token': os.environ['GITLABTOKEN'],
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Accept-Encoding': "gzip, deflate",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }
   
    response = requests.request("GET", apiurl, headers=headers)
    return json.loads(response.text)

