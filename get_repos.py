import json
from botocore.vendored import requests
import os

def get_repos(giturl):

    apiurl = "https://gitlab.com/api/graphql"

    headers = {
        'Private-Token': os.environ['GITLABTOKEN'],
        'Content-Type': "application/json",
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Accept-Encoding': "gzip, deflate",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }
    
    json_body = { 'query': '''query {
        group(fullPath: "''' + os.environ['GITLABGROUP']  +  '''") {
        projects(includeSubgroups: true) {
          edges {
            node {
              fullPath, id
            }
          }
        }
      }
    } '''}


    response = requests.request("POST", apiurl, headers=headers, json=json_body)
    json_object = json.loads(response.text)
    edges = json_object['data']['group']['projects']['edges']
    return edges

