from botocore.vendored import requests
import datetime

from get_repos import get_repos
from get_branches import get_branches
from download_branch import download_branch

import os


def handler(event, context):

    giturl = os.environ['GITURL']
    edges = get_repos(giturl)
    timestamp = datetime.datetime.utcnow().strftime("%Y-%m-%d_%H%M%S")
    
    for edge in edges:
        id = edge['node']['id'].replace('gid://gitlab/Project/', '')
        branches = get_branches(id)
        for branch in branches:
            download_branch(edge['node']['fullPath'], id, branch['name'], branch['commit']['id'], timestamp)
            print("Done: "+edge['node']['fullPath']+' for branch '+branch['name'])
    return { 
        'message' : 'All good!'
    }
